# Read every second letter code
import sys, random, string

# python readeverysecondletter.py <string> <encode|decode>
# python readeverysecondletter.py "I like apple" encode
# python readeverysecondletter.py "L#olnh#dssoh" decode
sentence = sys.argv[1]
option = sys.argv[2]

print("-"*40)
print(f"readeverysecondletter.py '{sentence}' {option}")
print("-"*40)

if option == 'encode':
    encoded = ""
    for c in sentence: 
        encoded += c + random.choice(list(string.ascii_lowercase))
    print(sentence, "-->", encoded)

if option == 'decode':
    decoded = ""
    for i in range(len(sentence)):
        if i%2 == 0:
            decoded += sentence[i]
    print(sentence, "-->", decoded)
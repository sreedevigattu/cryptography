----------------------------------------------------------------------------------------------------------------
## MAKE YOUR OWN CODE
Have a own code in which you will replace one letter with another character.
Given a word, the program should display the coded message. 
Given the coded msg, the program should display the original word.
    Sample input: #@@%^
    Expected output: apple if original = ['a','p','l','e'] --> code = ['#','@','%','^']
----------------------------------------------------------------------------------------------------------------
## CAESER CIPHER
Write a Python program to create a Caesar encryption. Go to the editor
Note : In cryptography, a Caesar cipher, also known as Caesar's cipher, the shift cipher, Caesar's code or Caesar 
shift, is one of the simplest and most widely known encryption techniques. It is a type of substitution cipher in 
which each letter in the plaintext is replaced by a letter some fixed number of positions down the alphabet. For 
example, with a left shift of 3, D would be replaced by A, E would become B, and so on. The method is named after 
Julius Caesar, who used it in his private correspondence.

References:
Introduction: https://www.youtube.com/watch?v=sMOZf4GN3oc
Online conversion: https://www.boxentriq.com/code-breaking/caesar-cipher 
ASCII Codes: https://www.rapidtables.com/code/text/ascii-table.html
----------------------------------------------------------------------------------------------------------------
## REVERSE CYPHER
----------------------------------------------------------------------------------------------------------------
## READEVERYSECONDLETTER
https://sites.google.com/site/codesforscouts/read-every-second-letter
----------------------------------------------------------------------------------------------------------------
## MORSE CODE
https://sites.google.com/site/codesforscouts/morse-code
----------------------------------------------------------------------------------------------------------------
## LETTERS to NUMBERS
----------------------------------------------------------------------------------------------------------------
## HACK THE CIPHER - The Brute-Force Attack
----------------------------------------------------------------------------------------------------------------
More on cryptography
https://www.khanacademy.org/computing/computer-science/cryptography/crypt/v/intro-to-cryptography
https://inventwithpython.com/hacking/
https://www.cerias.purdue.edu/education/k-12/teaching_resources/lessons_presentations/cryptology.html 
----------------------------------------------------------------------------------------------------------------
Encrypt content in a file
Multiplicative, affine cipher
TRANSPOSITION CIPHER
----------------------------------------------------------------------------------------------------------------
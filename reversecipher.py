import sys
# python reversecipher.py <string> <encode|decode>
# python reversecipher.py "i like apple" encode
# python reversecipher.py 'elppa ekil i' decode
sentence = sys.argv[1].lower()
option = sys.argv[2]
print("-"*40)
print(f"reversecipher.py '{sentence}' {option}")
print("-"*40)

print(sentence, "-->", sentence[::-1])
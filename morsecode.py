# Morse code
import sys

# python morsecode.py <string> <encode|decode>
# python morsecode.py "I like apple" encode
# python morsecode.py "L#olnh#dssoh" decode
sentence = sys.argv[1].lower()
option = sys.argv[2]

print("-"*40)
print(f"morsecode.py '{sentence}' {option}")
print("-"*40)

#               a   b       c       d   e   f       g      h    i       j   k       l     m     n   o
morsecodes = ['.-','-...','-.-.','-..','.','..-.','--.','....','..','.---','-.-','.-..','--', '-.','---',
#             p     q       r      s   t   u     v       w     x      y     z
            '.--.','--.-','.-.','...','-','..-','...-','.--','-..-','-.--','--..']

if option == 'encode':
    encoded = ""
    for c in sentence: 
        if c == " ":
            encoded += '//'
        else:
            encoded += morsecodes[ord(c)-97] + '/'
    print(sentence, "-->", ord(c)-97, chr(ord(c)-97), encoded)

if option == 'decode':
    decoded = ""
    prevpos = 0
    while '/' in sentence:
        letterpos = sentence.index('/')
        letter = sentence[:letterpos]
        prevpos = letterpos
        if letter == "":
            decoded += " "
        else:
            pos = morsecodes.index(letter)
            decoded += chr(pos+97)
        #print(sentence, letter, pos, decoded)
        sentence = sentence[letterpos+1:]
    decoded = decoded.replace("  "," ")
    print(sentence, "-->", decoded)
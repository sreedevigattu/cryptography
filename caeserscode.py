# Caeser's code
import sys

# -----------------------------------------
# Tests
# -----------------------------------------
# KEY--> 2 4 
# banana  dcpcpc ferere 
# apple crrng ettpi 
# apple-1 2-banana crrng-1 2-dcpcpc ettpi-1 2-ferere 
# monday oqpfca qsrhec 
# zombie bqodkg dsqfmi 
# APPLE apple CRRNG crrng ETTPI ettpi 
# MONDAY Monday OQPFCA oqpfca QSRHEC qsrhec 
# "nsn iye mbkmu dro myno?" 10 did you crack the code?

# python caeserscode.py <string> <encode|decode> <KEY>
# python caeserscode.py "I like apple" encode 3
# python caeserscode.py "L#olnh#dssoh" decode 3
sentence = sys.argv[1]
option = sys.argv[2]
KEY = int(sys.argv[3])

print("-"*40)
print(f"caeserscode.py '{sentence}' {option}")
print("-"*40)

FIRST_ALPHA_POS = ord('a')
LAST_ALPHA_POS = ord('a') + 26

if option == 'encode':
    encoded = ""
    for c in sentence: 
        if not c.isalpha():
            encoded += c
        else:
            newcpos = ord(c)+KEY
            #print(c, newcpos)
            if newcpos >= LAST_ALPHA_POS:
                newcpos = FIRST_ALPHA_POS + (newcpos - LAST_ALPHA_POS)
                #print(chr(newcpos), newcpos)
            encoded += chr(newcpos)
            print(c, newcpos, chr(newcpos))
    print(sentence, "-->", encoded)

if option == 'decode':
    decoded = ""
    for c in sentence: 
        if not c.isalpha():
            decoded += c
        else:
            newcpos = ord(c)-KEY
            if newcpos < FIRST_ALPHA_POS:
                newcpos = LAST_ALPHA_POS - (FIRST_ALPHA_POS - newcpos)
            #decoded += chr(ord(c)-KEY)
            decoded += chr(newcpos)
    print(sentence, "-->", decoded)
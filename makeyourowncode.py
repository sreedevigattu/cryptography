# Make your own code
import sys

# python makeyourowncode.py <string> <encode|decode>
# python makeyourowncode.py "i like apple" encode
# python makeyourowncode.py "& )&(# ~==)#" decode
sentence = sys.argv[1].lower()
option = sys.argv[2]
print("-"*40)
print(f"makeyourowncode.py '{sentence}' {option}")
print("-"*40)

alpha = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
codes = ['~','`','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[','}',']','|',':',';','>','?','<']
print("alpha", len(alpha))
print("codes", len(codes))
print(alpha[2], codes[2])

# print each alphabet and it's code
# Given a word, print the word and it's coded word.
# Take a coded word and print the original word.
encoded = ""
if option == "encode":
    for c in sentence:
        if c in alpha:
            i = alpha.index(c)
            encoded += codes[i]
        else:
            encoded += c
    print(sentence, encoded)

# Given a encoded message, what is the original message
decoded = ""
if option == "decode":
    for c in sentence:
        if c in codes:
            i = codes.index(c)
            decoded += alpha[i]
        else:
            decoded += c

    print(sentence, decoded)